// Dependencies
var express = require('express'),
    fs = require('fs');

var app = express();

// Middleware
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/public'));

// Routes
app.get('/', function (req, res) {
  res.render('index');
})

app.get('/api/list', function (req, res) {
  fs.readFile('data/data.json', function (err, data) {
    if (!err) {
      res.send(data);
    }
  })
})

// Run server
app.listen(3089, function () {
  console.log('Server is running on 3089');
});