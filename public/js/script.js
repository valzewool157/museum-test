$(document).ready(function () {

  //App class

  var App = function () {
    this.filters = {};
    this.data = [];
    this.page = 1;
    this.itemsOnPage = 10;
  };

  App.prototype.init = function (cb) {
    var self = this;
    self.getArtefactsList(function (err, data) {
      if (err) {
        console.log(err);
        self.data = [];
      } else {
        self.data = data;
      }
      cb();
    })
  }

  App.prototype.getArtefactsList = function (cb) {
    $.ajax({
      url: '/api/list',
      method: 'GET',
      dataType: 'json'
    })
    .fail(function (jqXHR, msg) {
      cb(msg);
    })
    .done(function (data) {
      cb(null, data.items);
    })
  }

  App.prototype.setFilter = function (name, value) {
    this.filters[name] = value;
    this.updateFilterButtons();
    this.renderTable();
  }

  App.prototype.resetFilter = function (name) {
    delete this.filters[name];
    this.updateFilterButtons();
    this.renderTable();
  }

  App.prototype.resetAllFilters = function () {
    this.filters = {};
    this.updateFilterButtons();
    this.renderTable();
  }

  App.prototype.setPage = function (page) {
    this.page = page;
    this.renderTable();
  }

  App.prototype.updateFilterButtons = function () {
    $('.filters__button').removeClass('filters__button_active')
    $.each(this.filters, function (key, value) {
      $('.filters__button[data-param="'+ key +'"]').addClass('filters__button_active');
    })
  }

  App.prototype.getFilteredList = function () {
    var data = $.extend([], this.data);

    $.each(this.filters, function (key, value) {
      if (key == 'date') {
        data = data.filter(function (item) { return (value >= item.Datebegin) && (value <= item.Datend); })
      }
      if (key == 'country') {
        data = data.filter(function (item) { return value == item.Country; })
      }
      if (key == 'place') {
        data = data.filter(function (item) { return value == item.Place; })
      }
      if (key == 'org') {
        data = data.filter(function (item) { return value == item.Org; })
      }
    })

    return data;
  }

  App.prototype.renderTable = function () {
    var self = this,
        list = self.getFilteredList(),
        $tableWrapper = $('.table__wrapper'),
        $tbody = $('.artefacts > tbody');

    $tbody.html('');

    self.renderPagination(list);

    list = list.splice((this.page - 1) * this.itemsOnPage, this.itemsOnPage);

    $.each(list, function (i, item) {
      self.renderRow(item);
    })

  }

  App.prototype.renderPagination = function (list) {
    var self = this,
        pagesCount,
        pagination = '';

    if (list.length > self.itemsOnPage) {
      pagesCount = Math.ceil(list.length / self.itemsOnPage);
      pagination += '<div class="btn-group" role="group">';
      for (var i = 1; i <= pagesCount; i++) {
        pagination += '<button type="button" class="btn btn-default table__pagination__button" data-page="' + i + '">' + i + '</button>';
      }
      pagination += '</div>';
      $('.table__pagination').html(pagination);
      $('.table__pagination__button').removeClass('table__pagination__button_active');
      $('.table__pagination__button[data-page="' + self.page + '"]').addClass('table__pagination__button_active');
    } else {
      $('.table__pagination').html('');
    }
  }

  App.prototype.renderRow = function (data) {
    var $tbody = $('.artefacts > tbody');
        row = '<tr class="artefacts__item" data-id="' + data.ID + '">\
                <td>'+ data.Name +'</td>\
                <td>'+ data.Place +'</td>\
                <td>'+ data.Date +'</td>\
                <td>'+ data.Org +'</td>\
              </tr>';
    $tbody.append(row);
  }

  App.prototype.showPopup = function (id) {
    var $modal = $('#artefacts__modal_info'),
        infoObj = this.data.find(function (item) { return item.ID == id; }),
        info = [];

    $modal.find('.artefact__name').text(infoObj.Name);
    $.each(infoObj, function (key, val) {
      info.push('<dl class="dl-horizontal">\
                  <dt>' + key + '</dt>\
                  <dd>' + val + '</dd>\
                </dl>')
    })
    $modal.find('.artefact__info').html(info.join(''));
    $modal.modal('show');
  }

  App.prototype.renderModal = function (type) {
    var self = this,
        $dateModal = $('#filters__modal_date'),
        $placeModal = $('#filters__modal_place'),
        $countryModal = $('#filters__modal_country'),
        $orgModal = $('#filters__modal_org')

    switch (type) {
      case 'date':
        $dateModal.find('.modal-body').html('<form class="form-group">\
            <input class="form-control" type="text" placeholder="Введите год" value="">\
            </form>');
        if (self.filters.date) {
          $dateModal.find('input').val(self.filters.date);
        }
        break;
      case 'place':
        var allPlaces = self.getFilteredList().map(function (item) { return item.Place }).unique(),
            select = '';

        select += '<select class="form-control">';
        $.each(allPlaces, function (i, place) {
          select += '<option value="' + place + '">' + place + '</option>';
        })
        select += '</select>';

        $placeModal.find('.modal-body').html(select);
        if (self.filters.place) {
          $placeModal.find('select').val(self.filters.place);
        }
        break;
      case 'country':
        var allCountries = self.getFilteredList().map(function (item) { return item.Country }).unique(),
            select = '';
        
        select += '<select class="form-control">';
        $.each(allCountries, function (i, country) {
          select += '<option value="' + country + '">' + country + '</option>';
        })
        select += '</select>';

        $countryModal.find('.modal-body').html(select);
        if (self.filters.country) {
          $countryModal.find('select').val(self.filters.country);
        }
        break;
      case 'org':
        var allOrgs = self.getFilteredList().map(function (item) { return item.Org }).unique(),
            select = '';
        
        select += '<select class="form-control">';
        $.each(allOrgs, function (i, org) {
          select += '<option value="' + org + '">' + org + '</option>';
        })
        select += '</select>';

        $orgModal.find('.modal-body').html(select);
        if (self.filters.org) {
          $orgModal.find('select').val(self.filters.org);
        }
        break;
    }
  }

  // Additional methods
  Array.prototype.contains = function(v) {
    for(var i = 0; i < this.length; i++) {
        if(this[i] === v) return true;
    }
    return false;
  };

  Array.prototype.unique = function() {
      var arr = [];
      for(var i = 0; i < this.length; i++) {
          if(!arr.contains(this[i])) {
              arr.push(this[i]);
          }
      }
      return arr; 
  }


  //run app

  var app = new App();

  app.init(function () {
    app.renderTable();
  })

  //events

  $(document).on('click', '.artefacts__item', function (e) {
    var $row = $(e.target).parents('tr'),
        id = $row.data().id;
    
    app.showPopup(id);
  })

  $(document).on('click', '.filters__button', function (e) {
    var type = $(e.target).data().param;
    app.renderModal(type);
  })

  $(document).on('click', '.table__pagination__button', function (e) {
    var page = $(e.target).data().page;
    app.setPage(page);
  })

  $(document).on('click', '.filters__button_resetAll', function () {
    app.resetAllFilters();
  })

  $(document).on('click', '.filter__modal__reset', function (e) {
    var $button = $(e.target),
        $modal = $button.parents('.modal'),
        type = $button.data().param;

    app.resetFilter(type);
    $modal.modal('hide');
  })

  $(document).on('click', '.filter__modal__apply', function (e) {
    var $button = $(e.target),
        $modal = $button.parents('.modal'),
        type = $button.data().param,
        value;

    switch (type) {
      case 'date':
        value = $modal.find('input').val();
        break;
      case 'place':
        value = $modal.find('select').val();
        break;
      case 'country':
        value = $modal.find('select').val();
        break;
      case 'org':
        value = $modal.find('select').val();
        break;
    }

    app.setFilter(type, value);

    $modal.modal('hide');
  })

})

